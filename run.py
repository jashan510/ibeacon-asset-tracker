#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import blescan
import sys
import datetime
import bluetooth._bluetooth as bluez
import requests


# from pubnub import Pubnub

# pubnub = Pubnub(publish_key='pub-c-93b65736-3719-49c3-9d0f-0a4c663a0495'
#                ,
#                subscribe_key='sub-c-f5826ca2-b194-11e5-bb8b-02ee2ddab7fe'
#                )
# channel = 'beacons'

# duplicate remove function

def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


dev_id = 0
try:
    sock = bluez.hci_open_dev(dev_id)
    print 'Scan started....'
except:

    print 'error accessing bluetooth device...'
    sys.exit(1)
ctr = 0

blescan.hci_le_set_scan_parameters(sock)
blescan.hci_enable_le_scan(sock)

while True:
    returnedList = blescan.get_major(sock, 90)
    returnedList = f7(returnedList)
    print '----------'
    print datetime.datetime.now()
    major = json.dumps(returnedList)
    print major
    while True:
    	try:
       		r = requests.post('http://erp.wickedride.com/ble/process.php',data=major)
    	except requests.exceptions.RequestException as e:    # This is the correct syntax
        	print e
        	continue
    	break    
    print r.text
    for beacon in returnedList:
        print beacon

#    pubnub.publish(channel=channel, message=major)
